/******************************************************************************

                            Onrequest C Compiler.
                Code, Compile, Run and Debug C program onrequest.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>
#include <string.h>
static int mark[20];      //Store the number resource that are "in used"
int i,j,np,nr; 		     //i,j are used for loop, np store number of processes, nr store number of resources 			
char request_val[100];   //Store  the resoures (R1,R2..) in the file
int count = 0;			//counter while reading file
int alloc[10][10],request[10][10],avail[10],w[10]; //Store actual allocation matrix 
														//Store actual available resources
														//Store request resource matrix
														//w is a temp array for storing available matrix
                       
int main()
{
printf("Hello World.\n");

//np=4;
//nr=6;
printf("Enter the no of process: ");fflush(stdout);
scanf("%d",&np);                                           //enter number of processes in the system
printf("Enter the no of resources: ");fflush(stdout);
scanf("%d",&nr);											// enter number of resources in the system

printf("\nInitializing request matrix\n");
for(i=0;i<np;i++){											//initialize request matrix as 0 
for(j=0;j<nr;j++){
request[i][j]=0;
}			
}

printf("Initializing allocation matrix\n");					//initialize allocation matrix as 0
for(i=0;i<np;i++){
for(j=0;j<nr;j++){
alloc[i][j]=0;
}
}

    
FILE *file = fopen("processes.txt", "r");    				//open file for reading
if(!file) {
    printf("Could not open file. Exiting application. Bye");   // check presence of file
}
else {
printf("Reading request matrix from file\n");          //build matrix fo request resources
while(!feof(file)) {
    fscanf(file,"%*s %*s %s ",request_val); //Get text   //read 3column only rowbyrow
	if (strstr(request_val,"R1")!= NULL){    //check if column value = R1
    request[count][0]=1;
    }
	if (strstr(request_val,"R2")!= NULL){   //check if column value = R2
    request[count][1]=1;
    }
	if (strstr(request_val,"R3")!= NULL){   //check if column value = R3
    request[count][2]=1;
    }
	if (strstr(request_val,"R4")!= NULL){ //check if column value = R4
    request[count][3]=1; 
    }
	if (strstr(request_val,"R5")!= NULL){ //check if column value = R5
    request[count][4]=1;
    }
	if (strstr(request_val,"R6")!= NULL){ //check if column value = R6
    request[count][5]=1;
    }
   count++;     //next row
} 
fclose(file);                                      //close file
printf("Reading alloc matrix from file\n");        //open file with pointer reading 0
FILE *file = fopen("processes.txt", "r");   //Get text   //read 3column only rowbyrow 
count=0;
while(!feof(file)) {
    fscanf(file,"%*s %s %*s ",request_val); //Get text  //read 2 column only rowbyrow
	if (strstr(request_val,"R1")!= NULL){     //check if column value = R1
    alloc[count][0]=1;
    }
	if (strstr(request_val,"R2")!= NULL){   //check if column value = R2
    alloc[count][1]=1;
    }
	if (strstr(request_val,"R3")!= NULL){   //check if column value = R3
    alloc[count][2]=1;
    }
	if (strstr(request_val,"R4")!= NULL){   //check if column value = R4
    alloc[count][3]=1;
    }
	if (strstr(request_val,"R5")!= NULL){   //check if column value = R5
    alloc[count][4]=1;
    }
	if (strstr(request_val,"R6")!= NULL){  //check if column value = R6
    alloc[count][5]=1;
    }
   count++;
}

printf("printing request matrix from file\n");       
for(i=0;i<np;i++){
for(j=0;j<nr;j++){
  printf("%d ", request[i][j]);                    //printing request matrix
}
printf("\n");
}

printf("printing alloc matrix from file\n");
for(i=0;i<np;i++){
for(j=0;j<nr;j++){
  printf("%d ", alloc[i][j]);                    //printing allocation matrix 
}
printf("\n");
}
/*start deadlock calculation below*/
for(j=0;j<nr;j++){
avail[j]=1;   									//initialize matrix available resource by 1
}

/*Available Resource calculation*/
for(i=0;i<np;i++){
for(j=0;j<nr;j++){
if(alloc[i][j]==1){							//if allocation resource =1; then available for that resource = 0
avail[j]=0;
}
}
}


printf("printing available resource matrix \n");
for(j=0;j<nr;j++)
{
 printf("%d ",avail[j]);  						//printing available resource;
}


//marking processes with zero allocation

for(i=0;i<np;i++)            				//for each process check each resource 
{
int count=0;
 for(j=0;j<nr;j++)							
   {
      if(alloc[i][j]==0)					
        count++;
      else
        break;
    }
 if(count==nr)
 mark[i]=1;								//if a resources has counter full (nr) then mark as 1 (full)
}
// initialize W with avail

for(j=0;j<nr;j++)
    w[j]=avail[j];						// assign avail matrix to temp matix w;

//mark processes with request less than or equal to W
for(i=0;i<np;i++)
{
int canbeprocessed=0;
 if(mark[i]!=1)
{
   for(j=0;j<nr;j++)					
    {
      if(request[i][j]<=w[j])
        canbeprocessed=1;						//check each requested resource in each process if available; then mark as canbeprocessed
      else
         {
         canbeprocessed=0;
         break;
          }
     }
if(canbeprocessed)
{
mark[i]=1;										//set mark for that resource as 1

for(j=0;j<nr;j++)
w[j]+=alloc[i][j];
}
}
}

//checking for unmarked processes
int deadlock=0;
for(i=0;i<np;i++)
if(mark[i]!=1)
deadlock=1;


if(deadlock)
printf("\n Deadlock detected");
else
printf("\n No Deadlock possible");






}//close of else
    return 0;
}
